import myChart from './utils/chart.js';
import { Component, OnInit, ViewChild } from '@angular/core';

import { TransformChartData } from './utils/chartTransformData';
import chartData from './data/chartData';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Stairway';
  chart;
  @ViewChild('canvas') canvas;

  ngOnInit() {
    const CTX = this.canvas.nativeElement.getContext('2d');
    this.chart = new myChart(CTX, TransformChartData(this.canvas.nativeElement, chartData));
  }

}
