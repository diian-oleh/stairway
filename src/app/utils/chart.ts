import Chart from 'chart.js';
import { _maxMinValue } from './chartTransformData';

const myChart = Chart;

myChart.plugins.register({
  id: 'AVG',
  // ...
  afterDatasetsDraw(chart) {
    const dataSets: Array<any> = chart.data.datasets;
    const scale = chart.scales;
    dataSets.forEach(el => {
      let sum, avg = 0;
      if (el.data.length) {
        sum = el.data.reduce(function (a, b) { return a + b; });
        avg = sum / el.data.length;
      }
      const onePercentPx = scale['y-axis-0'].maxHeight / (scale['y-axis-0'].max - scale['y-axis-0'].min);
      // draw avg
      if (avg) {
        const yPoint = scale['y-axis-0'].maxHeight -
          (onePercentPx * (avg - scale['y-axis-0'].min))
          + scale['y-axis-0'].paddingTop;

        chart.ctx.beginPath();
        chart.ctx.setLineDash([10, 12]);
        chart.ctx.moveTo(scale['x-axis-0'].left, yPoint);
        chart.ctx.lineWidth = el.borderWidth * 2;
        chart.ctx.strokeStyle = el.borderColor;
        chart.ctx.lineTo(scale['x-axis-0'].left + scale['x-axis-0'].width, yPoint);
        chart.ctx.stroke();

      }

      // draw sidebar
      const { min, max } = _maxMinValue(chart.data.datasets);
      // console.log(chart, { min, max }, chart.data.datasets);
      const sidebarMinPos = scale['y-axis-0'].maxHeight -
        (onePercentPx * (min - scale['y-axis-0'].min))
        + scale['y-axis-0'].paddingTop;

      const sidebarMaxPos = scale['y-axis-0'].maxHeight -
        (onePercentPx * (max - scale['y-axis-0'].min))
        + scale['y-axis-0'].paddingTop;

      // draw rect
      chart.ctx.fillStyle = 'rgb(254, 246, 237)';
      chart.ctx.fillRect(5, sidebarMaxPos,
        chart.options.layout.padding.left - 5, sidebarMinPos - sidebarMaxPos);


      chart.ctx.beginPath();
      chart.ctx.setLineDash([0, 0]);
      chart.ctx.lineWidth = 2;
      chart.ctx.strokeStyle = 'rgba(253, 214, 170,1)';
      chart.ctx.moveTo(4, sidebarMinPos);
      chart.ctx.lineTo(chart.options.layout.padding.left, sidebarMinPos);
      chart.ctx.stroke();

      chart.ctx.beginPath();
      chart.ctx.lineWidth = 2;
      chart.ctx.strokeStyle = 'rgba(253, 214, 170,1)';
      chart.ctx.moveTo(4, sidebarMaxPos);
      chart.ctx.lineTo(chart.options.layout.padding.left, sidebarMaxPos);
      chart.ctx.stroke();

      chart.ctx.beginPath();
      chart.ctx.lineWidth = 2;
      chart.ctx.strokeStyle = 'rgba(253, 214, 170,1)';
      chart.ctx.moveTo(5, sidebarMinPos);
      chart.ctx.lineTo(5, sidebarMaxPos);
      chart.ctx.stroke();
    });
  }
});



export default myChart;
