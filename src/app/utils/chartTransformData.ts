export function TransformChartData(canvas: HTMLCanvasElement, data) {

  const bouData = {
    labels: Array.from(new Array(MaxArraysLength(data)), function (_, i) {
      return i === 0 ? 1 : i;
    }),
    datasets: generateDatasets(canvas, data)
  };
  const bouOptions = {
    responsive: true,
    legend: {
      display: false,
      position: 'top'
    },
    layout: {
      padding: {
        left: 30,
        right: 0,
        top: 0,
        bottom: 0
      }
    },
    elements: {
      line: {
        tension: 0.3
      },
      point: {
        radius: 0
      }
    },
    scales: {
      xAxes: [{
        gridLines: false,
        ticks: {
          callback: function (tick, index) {
            return tick;
          }
        }
      }],
      yAxes: [{
        ticks: {
          suggestedMax: maxMinValue(data).max * 1.001,
          suggestedMin: maxMinValue(data).min * 0.999,
          callback: function (tick, index, ticks) {
            if (tick === 0) {
              return tick;
            }
            return tick + '\xB0';
          }
        }
      }]
    },
    hover: {
      mode: 'nearest',
      intersect: false
    },
    tooltips: {
      enabled: false,
      mode: 'nearest',
      intersect: false,
      custom: function (tooltip) {
        let tooltipEl: HTMLElement = document.getElementById('chartjs-tooltip');

        if (!tooltipEl) {
          tooltipEl = document.createElement('div');
          tooltipEl.id = 'chartjs-tooltip';
          tooltipEl.innerHTML = '<div id="title"></div><span id="triagle"></span>';
          this._chart.canvas.parentNode.appendChild(tooltipEl);
        }

        // Hide if no tooltip
        if (tooltip.opacity === '0') {
          tooltipEl.style.opacity = '0';
          return;
        }

        // Set caret Position
        tooltipEl.classList.remove('above', 'below', 'no-transform');
        if (tooltip.yAlign) {
          tooltipEl.classList.add(tooltip.yAlign);
        } else {
          tooltipEl.classList.add('no-transform');
        }

        function getBody(bodyItem) {
          return bodyItem.lines;
        }

        // Set Text
        if (tooltip.body) {
          const titleLines = tooltip.title || [];
          const bodyLines = tooltip.body.map(getBody);

          let innerHtml = '<div id="title">';

          titleLines.forEach(function (title) {
            innerHtml += '<span>' + title + '\xB0' + '</span>';
          });
          innerHtml += '</div>';

          const tableRoot = tooltipEl.querySelector('#title');
          tableRoot.innerHTML = innerHtml;
        }

        const positionY = this._chart.canvas.offsetTop;
        const positionX = this._chart.canvas.offsetLeft;

        if (tooltip.dataPoints) {
          const color = this._data.datasets[tooltip.dataPoints[0].datasetIndex].borderColor;
          tooltip.backgroundColor = color;
          tooltip.borderColor = color;
          tooltipEl.style.backgroundColor = color;
          tooltipEl.style.borderTopColor = color;
        }

        // Display, position, and set styles for font
        tooltipEl.style.opacity = '1';
        tooltipEl.style.left = positionX + tooltip.caretX + 'px';
        tooltipEl.style.top = positionY + tooltip.caretY - tooltipEl.clientHeight - 12 + 'px';
        tooltipEl.style.fontFamily = tooltip._bodyFontFamily;
        tooltipEl.style.fontSize = tooltip.bodyFontSize + 'px';
        tooltipEl.style.fontStyle = tooltip._bodyFontStyle;
      }
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        value: 95,
        borderColor: 'red',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
  };
  return {
    type: 'line',
    data: bouData,
    options: bouOptions
  };
}

function MaxArraysLength(data) {
  let length = 0;
  data.forEach(element => {
    if (element.data.length > length) {
      length = element.data.length;
    }
  });
  return length;
}


function generateDatasets(canvas: HTMLCanvasElement, data) {
  const datasets = [];
  data.forEach(element => {
    const set = {
      data: element.data,
      backgroundColor: RenderGradient(element.color, canvas),
      borderColor: element.color,
      pointBorderColor: element.color,
      pointBackgroundColor: '#fff',
      pointHoverBackgroundColor: element.color,
      borderWidth: 2,
      pointRadius: 4,
      pointHoverRadius: 6
    };

    datasets.push(set);
  });

  return datasets;

  function RenderGradient(rgbaColor: string, canvasEl: HTMLCanvasElement) {
    const colorsArr: Array<any> = rgbaColor.match(/(?<=rgba\()(.*)(?=\))/)[0].split(',');
    colorsArr[colorsArr.length - 1] = 0;
    const gradient = canvasEl.getContext('2d')
      .createLinearGradient(0, 0, 0, canvasEl.clientHeight);
    gradient.addColorStop(0, rgbaColor);
    gradient.addColorStop(1, `rgba(${colorsArr.join(',')})`);

    return gradient;
  }
}


function maxMinValue(bouData: any) {
  let max = null;
  let min = null;

  bouData.forEach(element => {
    const minOfArr = Math.min(...element.data);
    const maxOfArr = Math.max(...element.data);
    if (!min || (min > minOfArr)) {
      min = minOfArr;
    }
    if (!max) {
      max = maxOfArr;
    } else if (max < maxOfArr) {
      max = maxOfArr;
    }
  });
  return {
    max,
    min
  };
}

export const _maxMinValue = maxMinValue;
