const chartData = [
  {
    data: [86, 83, 86, 83, 87, 82, 84, 83],
    color: 'rgba(0,123,255,.5)'
  },
  {
    data: [60, 65, 62, 64, 65, 62, 65, 70],
    color: 'rgba(255,65,105,.5)'
  }
];

export default chartData;
